---
title: Squiid
images:
  - "/img/showcase/squiid-1.png"
  - "/img/showcase/squiid-2.png"
  - "/img/showcase/squiid-3.png"
  - "/img/showcase/squiid-4.png"
alts:
  - First Image
  - Second Image
  - Third Image
buttons:
  - get:
    text: Download Squiid
    class: btn btn-success text-white
    href: https://imaginaryinfinity.net/docs/squiid/getting_started/
    newtab: false
  - source:
    text: Source Code
    class: btn btn-primary text-white
    href: https://gitlab.com/ImaginaryInfinity/squiid-calculator/squiid
    newtab: false
  - docs:
    text: Documentation
    class: btn btn-danger text-white
    href: /docs/squiid
    newtab: false
layout: "software_page"
nav: false
---

Squiid is a modular calculator written in Rust. It is the successor to our previous calculator, ImaginaryInfinity Calculator.

#### Features
- Simple terminal user interface using Ratatui
- Supports both RPN and algebraic input
- Plugin support will be added in the future
