---
title: "Projects"
date: 2023-07-18T00:01:11-04:00
draft: false
software:
    - squiid:
        # preferred dimensions: 250 height x 180 width
        image: /img/squiidsquare_thumbnail.png
        title: Squiid
        description: Squiid is a modular calculator written in Rust. It is currently very early in development but it is intended to be the successor to ImaginaryInfinity Calculator.
        about_url: /projects/squiid
layout: "software_list"
nav: true
---